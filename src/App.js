import './App.css';
import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import Login from './Login.js';
import Dashboard from './Dashboard.js';
import Cookies from 'js-cookie';
import { correctName, correctPass, handleLogin as authHandleLogin } from './utils/auth.js';

const App = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isAuthenticated, setIsAuthenticated] = useState(false);

    useEffect(() => {
        const isLoggedIn = Cookies.get('isAuthenticated') === 'true';
        setIsAuthenticated(isLoggedIn);
    }, []);

    const handleLogin = () => {
        const isSuccessfulLogin = authHandleLogin(username, password, correctName, correctPass);

        if(isSuccessfulLogin) {
            setIsAuthenticated(true);
            Cookies.set('isAuthenticated', true);
        }
    };

    const handleLogout = () => {
        setIsAuthenticated(false);
        Cookies.set('isAuthenticated', false);
    }

    return (
        <Router>
            <Routes>
                <Route path='/login'
                element={<Login
                    username={username}
                    password={password}
                    setUsername={setUsername}
                    setPassword={setPassword}
                    onLogin={handleLogin}
                    />}
                />
                <Route path='/dashboard'
                element={isAuthenticated ? <Dashboard onLogout={handleLogout} /> : <Navigate to='/login' />}
                />
                <Route path='/*' element={<Navigate to='/login' />} />
            </Routes>
        </Router>
    );
}

export default App;
