import './App.css';
import React, { useEffect, useState } from 'react';

const Dashboard = () => {
    const [location, setLocation] = useState({});
    const [weatherData, setWeatherData] = useState([]);
    const [loading, setLoading] = useState(true);
    const apiKey = 'c52131535d4b366fae0edac8b9310a35';

    useEffect(() => {
        const getUserLocation = () => {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
            } else {
                console.log("Geolocation not supported");
            }
        };

        const successCallback = (position) => {
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;

            const geocodingApiUrl = `https://api.openweathermap.org/geo/1.0/reverse?lat=${latitude}&lon=${longitude}&limit=5&appid=${apiKey}`;

            fetch(geocodingApiUrl)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`HTTP error! Status: ${response.status}`);
                    }

                    return response.json();
                })
                .then(data => {
                    const city = data[0]?.name || 'Unknown';
                    setLocation({ latitude, longitude, city });

                    getWeatherByCity(city);
                })
                .catch(error => {
                    console.log(`Failed to fetch data: ${error.message}`);
                });
        };

        const errorCallback = (error) => {
            console.log(`Error getting user location: ${error.message}`);
        };

        const getWeatherByCity = (city) => {
            const weatherApiURL = `https://api.openweathermap.org/data/2.5/forecast?q=${city}&appid=${apiKey}`;

            setLoading(true);

            fetch(weatherApiURL)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(`HTTP error! Status: ${response.status}`);
                    }

                    return response.json();
                })
                .then(data => {
                    const filteredData = data.list.filter((entry, index) => {
                        const currentDate = new Date(entry.dt * 1000);
                        const nextEntry = data.list[index + 1];
                        const nextDate = nextEntry ? new Date(nextEntry.dt * 1000) : null;

                        return !nextDate || currentDate.getDate() !== nextDate.getDate();
                    })

                    setWeatherData(filteredData);
                })
                .catch(error => {
                    console.log(`Failed to fetch weather data: ${error.message}`);
                })
                .finally(() => {
                    setLoading(false);
                })
        };

        getUserLocation();
    }, [apiKey]);

    const getWeatherIcon = (weatherCode) => {
        const iconMappings = {
            '01d': '🌞', // clear sky (day)
            '01n': '🌙', // clear sky (night)
            '02d': '⛅', // few clouds (day)
            '02n': '☁️', // few clouds (night)
            '03d': '☁️', // scattered clouds (day)
            '03n': '☁️', // scattered clouds (night)
            '04d': '☁️', // broken clouds (day)
            '04n': '☁️', // broken clouds (night)
            '09d': '🌧️', // shower rain (day)
            '09n': '🌧️', // shower rain (night)
            '10d': '🌦️', // rain (day)
            '10n': '🌦️', // rain (night)
            '11d': '⛈️', // thunderstorm (day)
            '11n': '⛈️', // thunderstorm (night)
            '13d': '❄️', // snow (day)
            '13n': '❄️', // snow (night)
            '50d': '🌫️', // mist (day)
            '50n': '🌫️', // mist (night)
        };

        return iconMappings[weatherCode] || '❓';
    };

    return (
        <div className='Dashboard'>
            <h1>{location.city} Weather</h1>
            {loading ? (
                <p>Loading...</p>
            ) : (
                <div className="weather-entries">
                    {weatherData.map((entry, index) => (
                        <div key={index} className="weather-entry">
                            <p className='weather-icon'>
                                <span role="img" aria-label="weather-icon">
                                    {getWeatherIcon(entry.weather[0].icon)}
                                </span>
                            </p>
                            <p className='weather-temperature'>{Math.round(entry.main.temp - 273.15)}°C</p>
                            <p className='weather-date'>{new Date(entry.dt * 1000).toLocaleDateString()}</p>
                            <div className='weather-extra'>
                                <p>Feels like: {Math.round(entry.main.feels_like - 273.15)}°C</p>
                                <p>Min Temperature: {Math.round(entry.main.temp_min - 273.15)}°C</p>
                                <p>Max Temperature: {Math.round(entry.main.temp_max - 273.15)}°C</p>
                                <p>Pressure: {entry.main.pressure} hPa</p>
                                <p>Humidity: {entry.main.humidity}%</p>
                                <p>Weather: {entry.weather[0].description}</p>
                                <p>Wind Speed: {entry.wind.speed} m/s</p>
                            </div>
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default Dashboard;
