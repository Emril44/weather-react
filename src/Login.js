import './App.css';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { correctName, correctPass } from './utils/auth';

const Login = ({username, password, setUsername, setPassword, onLogin}) => {
    const [error, setError] = useState('');
    const navigate = useNavigate();
    
    const isLoginDisabled = !username || !password;

    const handleLogin = (e) => {
        e.preventDefault();

        onLogin();

        if(username === correctName && password === correctPass) {
            setError('');
            navigate('/dashboard');
        } else {
            setError('Please check if your login and password are correct.')
        }
    };

    return (
        <div className="Login-form">
            <h2>Log in to page</h2>
            <form onSubmit={handleLogin}>
                {error && <p className='error-message'>{error}</p>}
                <input className="Input-window" type='text' name='name' placeholder='Login' value={username} onChange={(e) => setUsername(e.target.value)}/>
                <input className="Input-window" type='password' name='pass' placeholder='Password' value={password} onChange={(e) => setPassword(e.target.value)}/>
                <input id='Login-button' type='submit' value="Log In" disabled={isLoginDisabled}/>
            </form>
        </div>
    );
}

export default Login;
